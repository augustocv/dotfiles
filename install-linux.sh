#!/usr/bin/env bash

install_safe () {
    if [ -f "$2" ]; then
        cmp --silent "$2" "$1" && echo -n "Skipping $1 - no modifications" && return

        read -p "$2 already exists. Backup and overwrite? (y/[n]) " -n 1 -r CONT
        if [ "$CONT" != "y" ]; then
            return;
        fi

        if [ ! -f "$2".original ]; then
            cp "$2" "$2".original 2> /dev/null
        fi

        mv "$2" "$2".old 2> /dev/null
    else
        echo "Copying $1 to $2..."
        DST_DIR=$(dirname -- "$2")
        if [ ! -d "$DST_DIR" ]; then echo "Creating parent dirs..."; fi
        mkdir -p "$DST_DIR"
    fi

    cp "$1" "$2"
}

install_safe _bash_aliases $HOME/.bash_aliases
echo
install_safe _gitconfig $HOME/.gitconfig
echo
install_safe _vimrc $HOME/.vimrc
echo
install_safe vimfiles/colors/xoria256.vim $HOME/.vim/colors/xoria256.vim
echo

if [ -z ${MSYSTEM+x} ]; then
    # Not Windows
    install_safe init.lua $HOME/.config/nvim/init.lua
    echo
    install_safe _my.zsh $HOME/.my.zsh
    echo
    install_safe _zprofile $HOME/.zprofile
    echo
    install_safe _tmux.conf $HOME/.tmux.conf
    echo
else
    # Windows
    install_safe init.lua $HOME/AppData/Local/nvim/init.lua
    echo
    install_safe Microsoft.PowerShell_profile.ps1 $HOME/Documents/WindowsPowerShell/Microsoft.PowerShell_profile.ps1
    echo
fi


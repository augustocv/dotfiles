--[[
Based on Kickstart.nvim

Requirements:
1. ripgrep for telescope
2. zig or C compiler for nvim-treesitter
3. zig for telescope-fzf-native.nvim
4. npm for pyright

To-Do: consider pyright + ruff_lsp
https://www.reddit.com/r/neovim/comments/11k5but/how_to_disable_pyright_diagnostics/
--]]

-- Set comma as the leader key
--  NOTE: Must happen before plugins are required (otherwise wrong leader will be used)
vim.g.mapleader = ','
vim.g.maplocalleader = ','

-- [[ Install `lazy.nvim` plugin manager ]]
--    https://github.com/folke/lazy.nvim
--    `:help lazy.nvim.txt` for more info
local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system {
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  }
end
vim.opt.rtp:prepend(lazypath)

-- [[ Configure plugins ]]
--  You can configure plugins using the `config` key.
--
--  You can also configure plugins after the setup call,
--    as they will be available in your neovim runtime.
require('lazy').setup({
  -- First, plugins that don't require any configuration

  -- Git related plugins
  'tpope/vim-fugitive',

  -- Detect tabstop and shiftwidth automatically
  'tpope/vim-sleuth',

  -- Quoting/parenthesizing helper
  'tpope/vim-surround',

  -- Repeat plugin commands with .
  'tpope/vim-repeat',

  -- Shell commands (e.g. :SudoWrite)
  'tpope/vim-eunuch',

  -- scrollbar
  'dstein64/nvim-scrollview',

  -- LSP Configuration & Plugins
  {
    'neovim/nvim-lspconfig',
    dependencies = {
      -- Automatically install LSPs to stdpath for neovim
      'williamboman/mason.nvim',
      'williamboman/mason-lspconfig.nvim',

      -- Useful status updates for LSP
      -- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
      -- To-Do: consider progress.suppress_on_insert
      { 'j-hui/fidget.nvim', opts = {} },

      -- Additional lua configuration, makes nvim stuff amazing!
      'folke/neodev.nvim',
    },
  },

  -- Autocompletion
  {
    'hrsh7th/nvim-cmp',
    dependencies = {
      -- Snippet Engine & its associated nvim-cmp source
      'L3MON4D3/LuaSnip',
      'saadparwaiz1/cmp_luasnip',

      -- LSP completion
      'hrsh7th/cmp-nvim-lsp',

      -- Command (:) completion
      'hrsh7th/cmp-path',
      'hrsh7th/cmp-cmdline',

      -- Adds a number of user-friendly snippets
      'rafamadriz/friendly-snippets',
    },
  },

  -- Useful plugin to show you pending keybinds.
  -- { 'folke/which-key.nvim', opts = {} },

  -- Adds git related signs to the gutter, as well as utilities for managing changes
  {
    'lewis6991/gitsigns.nvim',
    opts = {
      -- See `:help gitsigns.txt`
      signs = {
        add = { text = '+' },
        change = { text = '~' },
        delete = { text = '_' },
        topdelete = { text = '‾' },
        changedelete = { text = '~' },
      },
      on_attach = function(bufnr)
        vim.keymap.set('n', '<leader>hp', require('gitsigns').preview_hunk, { buffer = bufnr, desc = 'Preview git hunk' })

        -- don't override the built-in and fugitive keymaps
        local gs = package.loaded.gitsigns
        vim.keymap.set({ 'n', 'v' }, ']c', function()
          if vim.wo.diff then
            return ']c'
          end
          vim.schedule(function()
            gs.next_hunk()
          end)
          return '<Ignore>'
        end, { expr = true, buffer = bufnr, desc = 'Jump to next hunk' })
        vim.keymap.set({ 'n', 'v' }, '[c', function()
          if vim.wo.diff then
            return '[c'
          end
          vim.schedule(function()
            gs.prev_hunk()
          end)
          return '<Ignore>'
        end, { expr = true, buffer = bufnr, desc = 'Jump to previous hunk' })
      end,
    },
  },

  {
    -- Theme inspired by Atom
    'navarasu/onedark.nvim',
    priority = 1000,
    config = function()
      vim.cmd.colorscheme 'onedark'
    end,
  },

  {
    -- Set lualine as statusline
    'nvim-lualine/lualine.nvim',
    -- See `:help lualine.txt`
    opts = {
      options = {
        icons_enabled = false,
        theme = 'onedark',
        component_separators = '|',
        section_separators = '',
      },
      sections = {
        lualine_x = {}
      },
    },
  },

  -- Add indentation guides even on blank lines
  {
    'lukas-reineke/indent-blankline.nvim',
    -- See `:help ibl`
    main = 'ibl',
    opts = {},
  },

  -- gc/gcc comment toggle shortcuts
  { 'numToStr/Comment.nvim', opts = {} , lazy=false },

  -- Tagbar alternative
  {
    'stevearc/aerial.nvim',
    opts = {},
    keys = { { '<leader>tg', ':AerialToggle<cr>', mode = 'n', desc = 'Aerial tagbar', }, },
  },

  -- Fuzzy Finder (files, lsp, etc)
  {
    'nvim-telescope/telescope.nvim',
    branch = '0.1.x',
    dependencies = {
      'nvim-lua/plenary.nvim',
      -- Fuzzy Finder Algorithm which requires local dependencies to be built.
      -- Only load if `zig` is available.
      {
        'nvim-telescope/telescope-fzf-native.nvim',
        build = {'mkdir build ' .. (vim.fn.has('win32') == 1 and '-ea 0' or '-p'), 'zig cc -O3 -fpic -std=gnu99 -shared src/fzf.c -o build/libfzf.' .. (vim.fn.has('win32') == 1 and 'dll' or 'so')},
        cond = function()
          return vim.fn.executable 'zig' == 1
        end,
      },
    },
  },

  -- File browser (NERDTree alternative)
  {
    'nvim-telescope/telescope-file-browser.nvim',
    dependencies = { 'nvim-telescope/telescope.nvim', 'nvim-lua/plenary.nvim' }
  },

  -- Highlight, edit, and navigate code
  -- Requires C compiler
  {
    'nvim-treesitter/nvim-treesitter',
    dependencies = {
      'nvim-treesitter/nvim-treesitter-textobjects',
    },
    build = ':TSUpdate',
  },

  -- easymotion replacement
  {
    'ggandor/leap.nvim',
    config = function()
      vim.keymap.set({'n', 'x', 'o'}, 's', '<Plug>(leap-forward)')
      vim.keymap.set({'n', 'x', 'o'}, 'gs', '<Plug>(leap-backward)')
      vim.keymap.set({'n', 'x', 'o'}, '<leader>S', '<Plug>(leap-from-window)')
    end,
  },

  -- start screen
  {
    'mhinz/vim-startify',
    config = function()
      vim.keymap.set('n', '<leader>st', ':Startify<cr>')
      vim.g.startify_bookmarks = { {i = vim.api.nvim_get_runtime_file('init.lua', false)[1]}, }
      vim.g.startify_custom_header = ''
      vim.g.startify_list_order = {'files', 'bookmarks', 'sessions'}
      vim.g.startify_files_number = 20
      vim.g.startify_update_oldfiles = 1
      vim.g.startify_session_autoload = 1
      vim.g.startify_session_persistence = 1

    end,
  },

  -- formatter
  {
    'stevearc/conform.nvim',
    cmd = { 'ConformInfo' },
    keys = {
      {
        '<leader>ff',
        function()
          require('conform').format({ async = true, lsp_fallback = true })
        end,
        mode = '', desc = 'Format buffer',
      },
    },
    opts = {
      formatters_by_ft = {
        python = { 'black' },
      },
      -- custom args
      formatters = {
        black = {
          prepend_args = { '--line-length', '100', '--target-version', 'py310' },
        },
      },
    },
    init = function()
      vim.o.formatexpr = "v:lua.require'conform'.formatexpr()"
    end,
  },

  -- code refactor (:Refactor)
  {
    'ThePrimeagen/refactoring.nvim',
    dependencies = {
      'nvim-lua/plenary.nvim',
      'nvim-treesitter/nvim-treesitter',
    },
    config = function()
      require('refactoring').setup()
    end,
  },

  -- To-Do: setup if needed - Include guards helper for C/C++ header files (:HeaderguardAdd)
  -- 'vim-scripts/headerguard',

  -- To-Do: setup if needed - HTML markup helper (main binding: C-g,)
  -- 'mattn/emmet-vim',

}, {})

-- [[ Setting options ]]
-- See `:help vim.o`

-- Set highlight on search
vim.o.hlsearch = true
-- map ctrl+_ to clear current search
vim.keymap.set('n', '<c-_>', ":let @/ = ''<cr>", { silent = true } )

-- Make line numbers default
vim.wo.number = true

-- Enable mouse mode
vim.o.mouse = 'a'

-- Sync clipboard between OS and Neovim.
--  Remove this option if you want your OS clipboard to remain independent.
--  See `:help 'clipboard'`
vim.o.clipboard = 'unnamedplus'

-- Enable break indent
vim.o.breakindent = true

-- Save undo history
vim.o.undofile = false

-- Case-insensitive searching UNLESS \C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

-- Keep signcolumn on by default
vim.wo.signcolumn = 'yes'

-- whitespace
vim.o.listchars = 'tab:→ ,trail:·'
vim.opt.list = true

-- start scrolling before reaching the edge
vim.opt.scrolloff = 5

-- saner splitting (put new pane on the right or below)
vim.opt.splitbelow = true
vim.opt.splitright = true

-- folding
-- cheatsheet:
-- zc/zo/za: close/open/toggle current
-- zM/zR: close/open all
vim.opt.foldmethod = 'indent'
vim.opt.foldlevelstart = 99

-- disable 'swap files' (eg. .myfile.txt.swp)
vim.opt.swapfile = false

-- Decrease update time
vim.o.updatetime = 250
vim.o.timeoutlen = 300

-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menuone,noselect'

-- NOTE: You should make sure your terminal supports this
vim.o.termguicolors = true

-- [[ Basic Keymaps ]]

-- easier exit from insert mode
vim.keymap.set('i', 'kj', '<Esc>')

-- ctrl+n open new tab
vim.keymap.set('n', '<c-n>', ':tabnew<cr>')

-- ctrl+s save (note: may need to set 'stty stop undef' in .bashrc)
vim.keymap.set('i', '<c-s>', '<esc>:update<cr>')
vim.keymap.set('n', '<c-s>', ':update<cr>')

-- search selected text
vim.keymap.set('v', '<leader>se', '"hy/<c-r>h')

-- replace selected text
vim.keymap.set('v', '<leader>re', '"hy:%sno/<c-r>h//gc<left><left><left>')

-- grep selected text
vim.keymap.set('v', '<leader>gr', '"hy:grep! "<c-r>h"<cr>:copen<cr>')

-- move lines up/down
vim.keymap.set('n', '<leader>sw', ':m .-2<cr>==')
vim.keymap.set('n', '<leader>ws', ':m .+1<cr>==')
vim.keymap.set('v', '<leader>sw', ':m \'<-2<cr>gv=gv')
vim.keymap.set('v', '<leader>ws', ':m \'>+1<cr>gv=gv')

-- keep selection after indenting
vim.keymap.set('v', '>', '>gv')
vim.keymap.set('v', '<', '<gv')

-- change current window's working directory to the same of its file
vim.keymap.set('n', '<leader>cd', ':lcd %:p:h<cr>')

-- ctrl-hjkl as movement keys in insert mode
vim.keymap.set('i', '<c-h>', '<left>')
vim.keymap.set('i', '<c-j>', '<down>')
vim.keymap.set('i', '<c-k>', '<up>')
vim.keymap.set('i', '<c-l>', '<right>')

-- buffer cycling shortcuts
vim.keymap.set('n', '<leader>bn', ':bnext<cr>')
vim.keymap.set('n', '<leader>bp', ':bprevious<cr>')
vim.keymap.set('n', '<leader>bd', ':bdelete<cr>')

-- open/close quick fix window
vim.keymap.set('n', '[qf', ':copen<cr>')
vim.keymap.set('n', ']qf', ':cclose<cr>')

-- enable/disable spellcheck for english
vim.keymap.set('n', '[en', ':set spell spelllang=en_us<cr>')
vim.keymap.set('n', ']en', ':set nospell<cr>')

-- open/close error list
vim.keymap.set('n', ']er', ':lclose<cr>')

-- make
vim.keymap.set('n', '<leader>m', ':make<cr>')

-- vim-fugitive
vim.keymap.set('n', '<leader>gs', ':Git<cr>')

-- Diagnostic keymaps
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, { desc = 'Go to previous diagnostic message' })
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, { desc = 'Go to next diagnostic message' })
vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float, { desc = 'Open floating diagnostic message' })
vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist, { desc = 'Open diagnostics list' })

-- [[ Configure Telescope ]]
-- See `:help telescope` and `:help telescope.setup()`
require('telescope').setup {
  defaults = {
    mappings = {
      i = {
        ['<C-u>'] = require('telescope.actions').cycle_history_prev,
        ['<C-d>'] = require('telescope.actions').cycle_history_next,
        ['<esc>'] = require('telescope.actions').close,
      },
      n = {
        ['q'] = require('telescope.actions').close,
      },
    },
  },
}

-- Enable telescope fzf native, if installed
pcall(require('telescope').load_extension, 'fzf')

-- Telescope live_grep in git root
-- Function to find the git root directory based on the current buffer's path
local function find_git_root()
  -- Use the current buffer's path as the starting point for the git search
  local current_file = vim.api.nvim_buf_get_name(0)
  local current_dir
  local cwd = vim.fn.getcwd()
  -- If the buffer is not associated with a file, return nil
  if current_file == "" then
    current_dir = cwd
  else
    -- Extract the directory from the current file's path
    current_dir = vim.fn.fnamemodify(current_file, ":h")
  end

  -- Find the Git root directory from the current file's path
  local git_root = vim.fn.systemlist("git -C " .. vim.fn.escape(current_dir, " ") .. " rev-parse --show-toplevel")[1]
  if vim.v.shell_error ~= 0 then
    print("Not a git repository. Searching on current working directory")
    return cwd
  end
  return git_root
end

-- Custom live_grep function to search in git root
local function live_grep_git_root()
  local git_root = find_git_root()
  if git_root then
    require('telescope.builtin').live_grep({
      search_dirs = {git_root},
    })
  end
end

vim.api.nvim_create_user_command('LiveGrepGitRoot', live_grep_git_root, {})

vim.keymap.set('n', '<leader>?', require('telescope.builtin').oldfiles, { desc = '[?] Find recently opened files' })
vim.keymap.set('n', '<leader>fb', ':Telescope buffers sort_mru=true ignore_current_buffer=true<cr>', { desc = '[F]ind existing [b]uffers' })

vim.keymap.set('n', '<leader>/', function()
  -- You can pass additional configuration to telescope to change theme, layout, etc.
  require('telescope.builtin').current_buffer_fuzzy_find(require('telescope.themes').get_dropdown {
    winblend = 10,
    previewer = false,
  })
end, { desc = '[/] Fuzzily search in current buffer' })

vim.keymap.set('n', '<leader>gf', require('telescope.builtin').git_files, { desc = 'Search [G]it [F]iles' })
vim.keymap.set('n', '<leader>sf', require('telescope.builtin').find_files, { desc = '[S]earch [F]iles' })
vim.keymap.set('n', '<leader>sh', require('telescope.builtin').help_tags, { desc = '[S]earch [H]elp' })
vim.keymap.set('n', '<leader>ss', require('telescope.builtin').grep_string, { desc = '[S]earch current word' })
vim.keymap.set('n', '<leader>sg', require('telescope.builtin').live_grep, { desc = '[S]earch by [G]rep' })
vim.keymap.set('n', '<leader>sG', ':LiveGrepGitRoot<cr>', { desc = '[S]earch by [G]rep on Git Root' })
vim.keymap.set('n', '<leader>sd', require('telescope.builtin').diagnostics, { desc = '[S]earch [D]iagnostics' })
vim.keymap.set('n', '<leader>sr', require('telescope.builtin').resume, { desc = '[S]earch [R]esume' })

vim.keymap.set('n', '<leader>nt', ':Telescope file_browser path=%:p:h select_buffer=true<cr>', { desc = '[N]avigate [T]ree' })

-- [[ Configure Treesitter ]]
-- See `:help nvim-treesitter`
-- Defer Treesitter setup after first render to improve startup time of 'nvim {filename}'
vim.defer_fn(function()
  require('nvim-treesitter.configs').setup {
    -- Add languages to be installed here that you want installed for treesitter
    ensure_installed = { 'c', 'cpp', 'go', 'lua', 'python', 'rust', 'tsx', 'javascript', 'typescript', 'vimdoc', 'vim', 'bash' },

    -- Autoinstall languages that are not installed. Defaults to false (but you can change for yourself!)
    auto_install = false,

    -- Install parsers synchronously (only applied to `ensure_installed`)
    sync_install = false,

    -- List of parsers to ignore installing (or 'all')
    ignore_install = { '' },

    highlight = { enable = true },
    indent = { enable = true },
    incremental_selection = {
      enable = true,
      keymaps = {
        init_selection = '<c-space>',
        node_incremental = '<c-space>',
        scope_incremental = '<tab>',
        node_decremental = '<s-tab>',
      },
    },
    textobjects = {
      select = {
        enable = true,
        lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
        keymaps = {
          -- You can use the capture groups defined in textobjects.scm
          ['aa'] = '@parameter.outer',
          ['ia'] = '@parameter.inner',
          ['af'] = '@function.outer',
          ['if'] = '@function.inner',
          ['ac'] = '@class.outer',
          ['ic'] = '@class.inner',
        },
      },
      move = {
        enable = true,
        set_jumps = true, -- whether to set jumps in the jumplist
        goto_next_start = {
          [']m'] = '@function.outer',
          [']]'] = '@class.outer',
        },
        goto_next_end = {
          [']M'] = '@function.outer',
          [']['] = '@class.outer',
        },
        goto_previous_start = {
          ['[m'] = '@function.outer',
          ['[['] = '@class.outer',
        },
        goto_previous_end = {
          ['[M'] = '@function.outer',
          ['[]'] = '@class.outer',
        },
      },
      swap = {
        enable = true,
        swap_next = {
          ['<leader>a'] = '@parameter.inner',
        },
        swap_previous = {
          ['<leader>A'] = '@parameter.inner',
        },
      },
    },
  }
end, 0)

-- [[ Configure LSP ]]
--  This function gets run when an LSP connects to a particular buffer.
local on_attach = function(_, bufnr)
  local nmap = function(keys, func, desc)
    if desc then
      desc = 'LSP: ' .. desc
    end

    vim.keymap.set('n', keys, func, { buffer = bufnr, desc = desc })
  end

  nmap('<leader>rn', vim.lsp.buf.rename, '[R]e[n]ame')
  nmap('<leader>ca', vim.lsp.buf.code_action, '[C]ode [A]ction')

  nmap('gd', require('telescope.builtin').lsp_definitions, '[G]oto [D]efinition')
  nmap('gr', require('telescope.builtin').lsp_references, '[G]oto [R]eferences')
  nmap('gI', require('telescope.builtin').lsp_implementations, '[G]oto [I]mplementation')
  nmap('<leader>D', require('telescope.builtin').lsp_type_definitions, 'Type [D]efinition')
  nmap('<leader>td', require('telescope.builtin').lsp_document_symbols, '[T]elescope [D]ocument Symbols')
  nmap('<leader>tw', require('telescope.builtin').lsp_dynamic_workspace_symbols, '[T]elescope [W]orkspace Symbols')

  -- See `:help K` for why this keymap
  nmap('K', vim.lsp.buf.hover, 'Hover Documentation')
  nmap('<C-k>', vim.lsp.buf.signature_help, 'Signature Documentation')

  -- Lesser used LSP functionality
  nmap('gD', vim.lsp.buf.declaration, '[G]oto [D]eclaration')
  nmap('<leader>wa', vim.lsp.buf.add_workspace_folder, '[W]orkspace [A]dd Folder')
  nmap('<leader>wr', vim.lsp.buf.remove_workspace_folder, '[W]orkspace [R]emove Folder')
  nmap('<leader>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, '[W]orkspace [L]ist Folders')

  -- Create a command `:Format` local to the LSP buffer
  vim.api.nvim_buf_create_user_command(bufnr, 'Format', function(_)
    vim.lsp.buf.format()
  end, { desc = 'Format current buffer with LSP' })
end

-- document existing key chains
-- require('which-key').register {
--   ['<leader>c'] = { name = '[C]ode', _ = 'which_key_ignore' },
--   ['<leader>d'] = { name = '[D]ocument', _ = 'which_key_ignore' },
--   ['<leader>g'] = { name = '[G]it', _ = 'which_key_ignore' },
--   ['<leader>h'] = { name = 'More git', _ = 'which_key_ignore' },
--   ['<leader>r'] = { name = '[R]ename', _ = 'which_key_ignore' },
--   ['<leader>s'] = { name = '[S]earch', _ = 'which_key_ignore' },
--   ['<leader>w'] = { name = '[W]orkspace', _ = 'which_key_ignore' },
-- }

-- mason-lspconfig requires that these setup functions are called in this order
-- before setting up the servers.
require('mason').setup()
require('mason-lspconfig').setup()

-- Enable the following language servers
--  Feel free to add/remove any LSPs that you want here. They will automatically be installed.
--
--  Add any additional override configuration in the following tables. They will be passed to
--  the `settings` field of the server config. You must look up that documentation yourself.
--
--  If you want to override the default filetypes that your language server will attach to you can
--  define the property 'filetypes' to the map in question.
--
--  Debug:
--  :lua print(vim.inspect(vim.lsp.get_active_clients()))
local servers = {
  -- gopls = {},
  -- rust_analyzer = {},

  --[[
  jsconfig.json / tsconfig.json
{
  "compilerOptions": {
    "module": "CommonJS",
    "target": "ES6"
  },
  "exclude": ["node_modules"]
}
  ref: https://code.visualstudio.com/docs/languages/jsconfig
  --]]
  tsserver = {},

  html = {},

  --[[
  pyrightconfig.json
{
    "venvPath": "/path/to/project/",
    "venv": "venv",
    "pythonVersion": "3.10",
    "extraPaths": ["./third-party/"]
}
  --]]
  pyright = {},

  --[[
  .clangd
CompileFlags:
  CompilationDatabase: "build"

  cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -Bbuild -S.  # -G Ninja or -G "Unix Makefiles"
  or
  set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
  or
  ln -s build/compile_commands.json .

  ref: https://www.reddit.com/r/neovim/comments/17rhvtl/guide_how_to_use_clangd_cc_lsp_in_any_project/
  --]]
  clangd = {},

  lua_ls = {
    Lua = {
      workspace = { checkThirdParty = false },
      telemetry = { enable = false },
      -- NOTE: toggle below to ignore Lua_LS's noisy `missing-fields` warnings
      diagnostics = { disable = { 'missing-fields' } },
    },
  },
}

-- Setup neovim lua configuration
require('neodev').setup()

-- Comment default keymaps
require('Comment').setup()

-- nvim-cmp supports additional completion capabilities, so broadcast that to servers
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

-- Ensure the servers above are installed
local mason_lspconfig = require 'mason-lspconfig'

mason_lspconfig.setup {
  ensure_installed = vim.tbl_keys(servers),
}

mason_lspconfig.setup_handlers {
  function(server_name)
    local lspconfig_name = server_name == "tsserver" and "ts_ls" or server_name
    require('lspconfig')[lspconfig_name].setup {
      capabilities = capabilities,
      on_attach = on_attach,
      settings = servers[server_name],
      filetypes = (servers[server_name] or {}).filetypes,
    }
  end,
}

-- [[ Configure nvim-cmp ]]
-- See `:help cmp`
local cmp = require 'cmp'
local luasnip = require 'luasnip'
require('luasnip.loaders.from_vscode').lazy_load()
luasnip.config.setup {}

cmp.setup {
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  completion = {
    completeopt = 'menu,menuone,noinsert'
  },
  mapping = cmp.mapping.preset.insert {
    ['<C-n>'] = cmp.mapping.select_next_item(),
    ['<C-p>'] = cmp.mapping.select_prev_item(),
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete {},
    ['<CR>'] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
    ['<Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.expand_or_locally_jumpable() then
        luasnip.expand_or_jump()
      else
        fallback()
      end
    end, { 'i', 's' }),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.locally_jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { 'i', 's' }),
  },
  sources = {
    { name = 'nvim_lsp' },
    { name = 'luasnip' },
  },
}

-- Command (:) completion
cmp.setup.cmdline(':', {
  mapping = cmp.mapping.preset.cmdline(),
  sources = cmp.config.sources({
    { name = 'path' }
  }, {
    { name = 'cmdline' }
  }),
  matching = { disallow_symbol_nonprefix_matching = false }
})

-- ref: https://github.com/neovim/neovim/issues/13324#issuecomment-1592038788
-- try to disable diagnostics before file saving
vim.api.nvim_create_autocmd({"BufNew", "InsertEnter"}, {
-- or vim.api.nvim_create_autocmd({"BufNew", "TextChanged", "TextChangedI", "TextChangedP", "TextChangedT"}, {
  callback = function(args)
    vim.diagnostic.disable(args.buf)
  end
})
vim.api.nvim_create_autocmd({"BufWrite"}, {
  callback = function(args)
    vim.diagnostic.enable(args.buf)
  end
})

vim.diagnostic.config({
  virtual_text = { source = "always", },
  float = { source = "always", },
})

-- vim: ts=2 sts=2 sw=2 et

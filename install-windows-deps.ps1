# use ~\opt for packages and ~\bin for single executables (~\bin should be in PATH)
mkdir $HOME\opt -ea 0
mkdir $HOME\bin -ea 0

# To-Do: review PowerShell modules installation

$choice_idx = $Host.UI.PromptForChoice('powershell modules', 'install?', @('&Yes'; '&No'), 1)

if ($choice_idx -eq 0) {

    Install-Module PSReadLine -Scope CurrentUser -Force # last tested: 2.3.4
    Install-Module posh-git -Scope CurrentUser  # last tested: 1.1.0

}

# Vim plugin manager (assuming Vim from Git Bash; :PlugInstall needs to run from Git Bash)
$choice_idx = $Host.UI.PromptForChoice('vim-plug', 'install?', @('&Yes'; '&No'), 1)
if ($choice_idx -eq 0) {
    curl.exe -fLo $HOME\.vim\autoload\plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
}

# Scoop (package manager)
# Update with `scoop update -a`
$choice_idx = $Host.UI.PromptForChoice('scoop + deps', 'install?', @('&Yes'; '&No'), 1)

if ($choice_idx -eq 0) {

    curl.exe -fSLo .\scoop-install.ps1 https://raw.githubusercontent.com/scoopinstaller/install/master/install.ps1
    .\scoop-install.ps1 -ScoopDir $HOME\opt\scoop

    # note: follow bat+less issue and unpin @643 when solved: https://github.com/gwsw/less/issues/538
    scoop install main/ninja main/eza main/aria2 main/bat main/ripgrep main/uv main/cmake main/nodejs-lts main/zig main/neovim main/fd main/less@643

} else {

    # WinGet (package manager)
    # Update with `winget  upgrade --all`
    $choice_idx = $Host.UI.PromptForChoice('winget deps', 'install?', @('&Yes'; '&No'), 1)

    if ($choice_idx -eq 0) {
        # note: skipped following installer-based (i.e., global and clunky) packages: Kitware.CMake, OpenJS.NodeJS.LTS, Neovim.Neovim
        winget install eza-community.eza aria2.aria2 sharkdp.bat BurntSushi.ripgrep.MSVC astral-sh.uv
        winget install jftuga.less --version 643
    }

}

# Miniforge (C++ libraries and optimized Python dependencies)
# Call `condahookps1` alias followed by `conda env list`, `conda activate <env_name>`, ...
# Update with `conda update -n base -c conda-forge -c nodefaults --all` (or check channels in ~\.condarc)
$choice_idx = $Host.UI.PromptForChoice('miniforge', 'install?', @('&Yes'; '&No'), 1)

if ($choice_idx -eq 0) {

    curl.exe -fSLo $HOME\opt\miniforge.exe https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-Windows-x86_64.exe

    start-process $HOME\opt\miniforge.exe -Wait -ArgumentList /InstallationType=JustMe,/RegisterPython=0,/AddToPath=0,/NoRegistry=1,/S,/D=$HOME\opt\miniforge

}

#!/usr/bin/env bash

mkdir -p ~/opt ~/bin

read -p "Install APT packages? (y/[n]) " -n 1 -r CONT; echo
if [ "$CONT" == "y" ]; then
    # ppa packages
    sudo apt update

    # distro packages
    sudo apt install git git-lfs vim zsh silversearcher-ag tmux xclip exuberant-ctags curl build-essential unzip bzip2
fi

# vim plugin manager
read -p "Install vim-plug? (y/[n]) " -n 1 -r CONT; echo
if [ "$CONT" == "y" ]; then
    curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi

# zimfw for zshell
read -p "Install zimfw? (y/[n]) " -n 1 -r CONT; echo
if [ "$CONT" == "y" ]; then
    curl -fL https://raw.githubusercontent.com/zimfw/install/master/install.zsh | zsh
    # custom additional settings added by install-linux.sh
    echo "[ -f ~/.my.zsh ] && source ~/.my.zsh" >> ~/.zshrc
fi

# mise for local package management
read -p "Install mise + deps? (y/[n]) " -n 1 -r CONT; echo
if [ "$CONT" == "y" ]; then
    curl -fLo ~/.local/bin/mise --create-dirs https://mise.jdx.dev/mise-latest-linux-x64
    chmod u+x ~/.local/bin/mise
    echo -e '\nPATH="$HOME/.local/share/mise/shims:$PATH"' >> ~/.profile
    MISE_YES=1 ~/.local/bin/mise use ninja eza bat ripgrep uv cmake node@20 zig neovim fd fzf
fi

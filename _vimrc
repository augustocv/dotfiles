set nocompatible " be iMproved, required

" leader key as comma
let mapleader = ','

" easier exit from insert mode
:imap kj <esc>

" color scheme
if (has('gui_running') || &t_Co == 256) && findfile("colors/xoria256.vim", &rtp) != ""
  :silent! colorscheme xoria256
  let g:indent_guides_auto_colors = 0
  autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=grey15 ctermbg=235
  autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=grey20 ctermbg=236
  " unset background to fix transparency:
  " :hi Normal guibg=NONE ctermbg=NONE
else
  :silent! colorscheme desert
endif

" fix background in tmux
if &term =~ '256color'
  set t_ut=
endif

" gui setup
if has('gui_running')
  set guioptions-=T " remove toolbar
  set guioptions-=r " remove scrollbars
  set guioptions-=l " remove scrollbars
  set guioptions-=e " remove tabs
  set guioptions-=m " remove tabs
  nnoremap <c-f1> :if &go=~#'m'<bar>set go-=m<bar>else<bar>set go+=m<bar>endif<cr>
endif

" open file dialog shortcut
nnoremap <leader>of :browse confirm e<cr>

" save file as dialog shortcut
nnoremap <leader>sa :browse confirm w<cr>

" disable file type based filters in file dialog
autocmd FileType * let b:browsefilter = ''

" mouse in terminal
if !has('gui_running')
  set mouse=a
endif

" syntax highlight
syntax on

" set template definition files as C++
au BufNewFile,BufRead *.txx set filetype=cpp
" set CUDA header files as C++
au BufNewFile,BufRead *.cuh set filetype=cpp

" line numbers
set number

" ignore case when opening files
set wildignorecase

" show command line completion options in status
set wildmenu

" search settings
set ignorecase
set smartcase
set incsearch
set hlsearch
" map ctrl+_ to clear current search
noremap <silent> <c-_> :let @/ = ''<cr>

" tab settings
" hard 4 columns wide
nnoremap <leader>tb :set noet sw=4 sts=4 ts=4<cr>
" soft 4 columns wide
nnoremap <leader>sp :set et sw=4 sts=4<cr>
" soft 2 columns wide
nnoremap <leader>s2 :set et sw=2 sts=2<cr>

" fix backspace for gvim on windows
set backspace=indent,eol,start

" display encoding utf-8
set encoding=utf-8

" whitespace
set list listchars=tab:→\ ,trail:·

" prevent 'tilde backup files' (eg. myfile.txt~)
set nobackup

" Uncomment below to cause 'tilde backup files' to be created in a different dir so as not to clutter up the current file's directory (probably a better idea than disabling them altogether)
if has('win32') || has('win64')
  set backupdir=C:\Windows\Temp
elseif has('win32unix')
  set backupdir=/c/Windows/Temp
else
  set backupdir=/tmp
endif
" disable 'swap files' (eg. .myfile.txt.swp)
set noswapfile

" change current window's working directory to the same of its file
nnoremap <leader>cd :lcd %:p:h<cr>

" paste mode on/off toggle in insert mode
set pastetoggle=<f3>

" copy/paste shortcuts
if has('win32') || has('win64')
  vnoremap <c-x> "*x
  vnoremap <c-c> "*y
  vnoremap <c-v> "*gP
  inoremap <c-v> <c-r><c-o>*
  cnoremap <c-v> <c-r><c-o>*
else
  vnoremap <c-x> "+x
  vnoremap <c-c> "+y
  vnoremap <c-v> "+gP
  inoremap <c-v> <c-r><c-o>+
  cnoremap <c-v> <c-r><c-o>+
endif

" ctrl+n open new tab
nnoremap <c-n> :tabnew<cr>
" ctrl+s save (note: may need to set 'stty stop undef' in .bashrc)
inoremap <c-s> <esc>:update<cr>
nnoremap <c-s> :update<cr>

" move lines up/down
nnoremap <leader>sw :m .-2<cr>==
nnoremap <leader>ws :m .+1<cr>==
vnoremap <leader>sw :m '<-2<cr>gv=gv
vnoremap <leader>ws :m '>+1<cr>gv=gv

" status bar
set laststatus=2

" show command being typed and size of selection on the bottom right
set showcmd

" search selected text
vmap <leader>se "hy/<c-r>h

" replace selected text
vmap <leader>re "hy:%sno/<c-r>h//gc<left><left><left>

" grep selected text
vmap <leader>gr "hy:grep! "<c-r>h"<cr>:copen<cr>

" ctrl-hjkl as movement keys in insert mode
imap <c-h> <left>
imap <c-j> <down>
imap <c-k> <up>
imap <c-l> <right>

" disable bell/alert
set noeb novb

" start scrolling before reaching the edge
set scrolloff=5

" saner splitting (put new pane on the right or below)
set splitbelow
set splitright

" folding
" cheatsheet:
" zc/zo/za: close/open/toggle current
" zM/zR: close/open all
set foldmethod=indent
set foldlevelstart=99

" tags (read tags file from current file directory upwards)
set tags=./tags;

" buffer cycling shortcuts
nmap <leader>bn :bnext<cr>
nmap <leader>bp :bprevious<cr>
nmap <leader>bd :bdelete<cr>

" source open file (e.g. vimrc)
nmap <f5> :so%<cr>

" open/close quick fix window
nmap [qf :copen<cr>
nmap ]qf :cclose<cr>

" auto-close preview window
autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

" enable/disable spellcheck for english
nmap [en :set spell spelllang=en_us<cr>
nmap ]en :set nospell<cr>

" open/close error list
nmap [er :Error<cr>
nmap ]er :lclose<cr>

" ------------- vim-polyglot
let g:polyglot_disabled = ['python']

" ------------- vim-plug begin

" curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

if empty(globpath(&rtp, 'autoload/plug.vim'))
  finish
endif

" reuse old Vundle plugin directory
if has('win32') || has('win64') || has('win32unix')
  let path='~/vimfiles/bundle'
else
  let path='~/.vim/bundle'
endif
call plug#begin(path)

" file navigation
Plug 'ctrlpvim/ctrlp.vim'

" static analysis
Plug 'vim-syntastic/syntastic'

" file tree
Plug 'scrooloose/nerdtree', { 'on': ['NERDTreeToggle', 'NERDTreeCWD'] }

" start screen
Plug 'mhinz/vim-startify'

" extensible & universal comment
Plug 'tomtom/tcomment_vim'

" lean & mean status/tabline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Vim motions on speed
Plug 'easymotion/vim-easymotion'

" Motion through camelCase and under_score (,w ,b ,e)
Plug 'bkad/CamelCaseMotion'

" Include guards helper for C/C++ header files (:HeaderguardAdd)
Plug 'vim-scripts/headerguard'

" Snippets engine + dependencies
Plug 'MarcWeber/vim-addon-mw-utils'
Plug 'tomtom/tlib_vim'
Plug 'garbas/vim-snipmate'

" Snippets library
Plug 'honza/vim-snippets'

" HTML markup helper (main binding: C-g,)
Plug 'mattn/emmet-vim'

" indentation guides
Plug 'nathanaelkane/vim-indent-guides'

" Tags display
" dependencies: exuberant-ctags
Plug 'majutsushi/tagbar'

" Detect indentation
Plug 'Raimondi/YAIFA'

" Syntax pack
Plug 'sheerun/vim-polyglot'

" Quoting/parenthesizing helper
Plug 'tpope/vim-surround'

" Repeat plugin commands with .
Plug 'tpope/vim-repeat'

" Shell commands (e.g. :SudoWrite)
Plug 'tpope/vim-eunuch'

" Git helpers
Plug 'tpope/vim-fugitive'

" Distraction-free writing
Plug 'junegunn/goyo.vim'

call plug#end()

" ------------- vim-plug end

" -------------- syntastic
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_mode_map = { 'mode': 'passive', 'active_filetypes': [], 'passive_filetypes': [] }

nmap [sy :SyntasticCheck<cr>
nmap ]sy :SyntasticReset<cr>

" c++
let g:syntastic_cpp_check_header = 1
let g:syntastic_cpp_compiler_options = '-std=c++11'

" flake8: ignore PEP8 checkings:
" - E402: module level import not at top of file
" - D103: Missing docstring in public function
let g:syntastic_python_flake8_args = '--max-line-length=100 --ignore=E402,D103'
" pylint options
let g:syntastic_python_pylint_args = '--extension-pkg-whitelist=cv2 --disable=wrong-import-position --disable=missing-docstring'

let g:airline#extensions#syntastic#enabled = 1

let g:syntastic_javascript_checkers = ['jshint']

" -------------- startify
let g:startify_bookmarks = [ {'v': '~/.vimrc'} ]
nnoremap <leader>st :Startify<cr>
let g:startify_custom_header = []
let g:startify_list_order = ['files', 'bookmarks', 'sessions']
let g:startify_files_number = 20
let g:startify_update_oldfiles = 1

" -------------- airline
if has('gui_running')
  let g:airline_theme='molokai'
else
  let g:airline_theme='badwolf'
endif
let g:airline#extensions#whitespace#enabled = 0
let g:airline_left_sep = ''
let g:airline_right_sep = ''

" -------------- snipmate
:imap <c-t>e <Plug>snipMateNextOrTrigger
:smap <c-t>e <Plug>snipMateNextOrTrigger
let g:snipMate = { 'snippet_version' : 1 }

" ------------- headerguard

" include underline in the end of the macro
function! g:HeaderguardName()
  return toupper(expand('%:t:gs/[^0-9a-zA-Z_]/_/g')) . '_'
endfunction

" change C-style comments (/**/) to C++-style (//)
function! g:HeaderguardLine3()
  return '#endif // ' . g:HeaderguardName()
endfunction

" ------------- ctrlp

" Setup ctrlp (& vim) to use ag search.
if executable('ag')
  set grepprg=ag\ --nogroup\ --nocolor
  if has('unix')
    let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
  else
    let g:ctrlp_user_command = 'ag -l --nocolor -g "" %s'
  endif
  let g:ctrlp_use_caching = 0
endif

let g:ctrlp_by_filename = 1
let g:ctrlp_regexp = 1
let g:ctrlp_lazy_update = 1 " update only when done typing
let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git\|dist/*'
let g:ctrlp_max_files = 0 " unlimited
let g:ctrlp_max_depth = 100
let g:ctrlp_extensions = ['tag', 'buffertag']
let g:ctrlp_match_window = 'bottom,order:btt,min:1,max:10,results:50'

nnoremap <leader>ft :CtrlPTag<cr>
nnoremap <leader>fb :CtrlPBuffer<cr>

" ------------- vim-indent-guides
let g:indent_guides_start_level = 1
let g:indent_guides_guide_size = 1
let g:indent_guides_enable_on_vim_startup = 1

" ------------- emmet-vim
let g:user_emmet_leader_key = '<c-g>'

" ------------- camelcasemotion
:silent! call camelcasemotion#CreateMotionMappings('<leader>')

" ------------- nerdtree
nnoremap <leader>nt :NERDTreeToggle<cr>
nnoremap <leader>nd :NERDTreeCWD<cr>
let g:NERDTreeQuitOnOpen = 1

" ------------- tagbar
nnoremap <leader>tg :TagbarOpenAutoClose<cr>

" ------------- yaifa
let g:yaifa_tab_width = 4
" default to spaces
let g:yaifa_indentation = 0

" ------------- vim-fugitive
nmap <leader>gs :Git<cr>

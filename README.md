# dotfiles

## Helpful commands

### Generate SSH key

    ssh-keygen -t ed25519 -b 4096 -C "{username@emaildomain.com}" -f {ssh-key-name}

### Clone from Bitbucket via SSH on port 443

    git clone ssh://git@altssh.bitbucket.org:443/augustocv/dotfiles .config/dotfiles.bitbucket

### Zsh

    chsh -s /bin/zsh

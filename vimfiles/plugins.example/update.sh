#!/usr/bin/env bash

BASEDIR=$(dirname "$0")

# ctags -f $BASEDIR/opencv_tags -R --exclude=3rdparty --exclude=doc --exclude=test --exclude=misc --exclude=perf --exclude=CMakeLists.txt --exclude=java --exclude=python --exclude='*cuda*' --exclude=ts --c++-kinds=+p --fields=+iaS --extra=+q  ~/src/opencv/modules/

# ctags -f $BASEDIR/gtest_tags ~/src/gtest/googletest/include/gtest/*.h ~/src/gtest/googletest/src/*.cc

ctags -f $BASEDIR/my_tags -R --exclude=.git --exclude=build --exclude=docs --c++-kinds=+p --fields=+iaS --extra=+q /path/to/my_project/

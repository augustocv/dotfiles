" consider using the latest version:
" https://llvm.org/svn/llvm-project/cfe/trunk/tools/clang-format/clang-format.py
let g:clang_format_fallback_style="google"
map <leader>cl :pyfile /usr/share/vim/addons/syntax/clang-format-3.9.py<cr>

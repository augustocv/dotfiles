# aliases
if [[ -s "$HOME/.bash_aliases" ]]; then
  source "$HOME/.bash_aliases"
fi

# disable stop signal on ctrl+s
stty stop undef
# disable start signal on ctrl+q
stty start undef

# alt-u to change to parent directory
bindkey -s '\eu' '^Ucd ..^M'

# alt-p rewinds directory stack (if AUTO_PUSHD is set)
# bindkey -s '\ep' '^Upopd >/dev/null; dirs -v^M'
bindkey -s '\ep' '^Upushd -q +1^M'

# alt-n forwards directory stack (if AUTO_PUSHD is set)
bindkey -s '\en' '^Upushd -q -0^M'

# alt-l to add pipe through less
bindkey -s "\el" " 2>&1|less"

# disable glob for pip to allow package[option] syntax
alias pip='noglob pip'

# direnv
if [ -x "$(command -v direnv)" ]; then
  eval "$(direnv hook zsh)"
fi

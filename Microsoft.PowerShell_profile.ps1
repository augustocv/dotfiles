# Location: ~\Documents\WindowsPowerShell

# PSReadLine options
Set-PSReadLineOption -EditMode Emacs
Set-PSReadLineOption -PredictionViewStyle ListView
Set-PSReadlineKeyHandler -Key Tab -Function MenuComplete

# git completion (disabled due to slow startup)
# Import-Module posh-git

# Vim aliases (assuming Vim from Git Bash)
$vim_exe = join-path (gi (gcm git).source).directory.parent.fullname "usr\bin\vim.exe"
New-Alias vi "$vim_exe"
New-Alias vim "$vim_exe"

# conda aliases
New-Alias condahookps1 "$home\opt\miniforge\shell\condabin\conda-hook.ps1"

# eza aliases (better ls)
New-Alias l 'eza'
function ll { & eza -al --group-directories-first $args }

# git aliases
function gst { & git status $args }
function gdf { & git diff $args }
function gdfc { & git diff --cached $args }

# git prompt
# ref: https://stackoverflow.com/a/44411205
function Write-BranchName {
    try {
        $branch = git rev-parse --abbrev-ref HEAD

        if ($branch -eq "HEAD") {
            # we're probably in detached HEAD state, so print the SHA
            $branch = git rev-parse --short HEAD
            Write-Host " ($branch)" -ForegroundColor Red -NoNewline
        } else {
            # we're on an actual branch, so print it
            Write-Host " ($branch)" -ForegroundColor Green -NoNewline
        }
    } catch {
        # we'll end up here if we're in a newly initiated git repo
        Write-Host " (no branches yet)" -ForegroundColor Yellow -NoNewline
    }
}

# custom prompt:
# user in C:\some\dir (master)
# PS> commands
function prompt {
    # ref: https://learn.microsoft.com/en-us/windows/terminal/tutorials/new-tab-same-directory
    $loc = $executionContext.SessionState.Path.CurrentLocation;
    if ($loc.Provider.Name -eq "FileSystem") {
      write-host "$([char]27)]9;9;`"$($loc.ProviderPath)`"$([char]27)\"
    }

    write-host "$env:username" -ForegroundColor DarkMagenta -NoNewline
    write-host " in " -NoNewline
    write-host "$loc" -ForegroundColor DarkGray -NoNewline
    if (Test-Path .git) { Write-BranchName }
    write-host ""
    write-host "PS>" -NoNewline
    return " "
}

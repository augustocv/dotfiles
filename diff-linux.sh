#!/usr/bin/env bash

run_diff () {
    OUTPUT="$(git diff $1 $2)"
    if [[ ! -z "$OUTPUT" ]]; then
        git -c core.pager='less -+$LESS -+F -RXSc' diff $1 $2
    fi
}

run_diff _bash_aliases $HOME/.bash_aliases
run_diff _gitconfig $HOME/.gitconfig
run_diff _vimrc $HOME/.vimrc

if [ -z ${MSYSTEM+x} ]; then
    # Not Windows
    run_diff init.lua $HOME/.config/nvim/init.lua
    run_diff _my.zsh $HOME/.my.zsh
    run_diff _zprofile $HOME/.zprofile
    run_diff _tmux.conf $HOME/.tmux.conf
else
    # Windows
    run_diff init.lua $HOME/AppData/Local/nvim/init.lua
    run_diff Microsoft.PowerShell_profile.ps1 $HOME/Documents/WindowsPowerShell/Microsoft.PowerShell_profile.ps1
fi
